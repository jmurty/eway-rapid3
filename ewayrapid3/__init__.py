from base64 import b64encode
import json
import urlparse
import requests
from ewayrapid3.lib import EwayObject


class EWayError(Exception):

    def __init__(self, msg, json_data=None):
        self.json_data = json_data
        super(EWayError, self).__init__(msg)


class Customer(EwayObject):
    pass

class ShippingAddress(EwayObject):
    pass

class Item(EwayObject):
    pass

class Option(EwayObject):
    pass

class Payment(EwayObject):
    pass

class API(object):
    cache_responses = False
    cache = {}

    def __init__(self, key=None, secret=None, sandbox=True):
        if not key or not secret:
            raise Exception(
                'The Eway API requires your API key and password'
            )
        self.key = key
        self.secret = secret
        if sandbox:
            self.url_prefix = 'https://api.sandbox.ewaypayments.com/'
        else:
            self.url_prefix = 'https://api.ewaypayments.com/'
        auth = '%s:%s' % (key,secret)
        auth64 = b64encode(auth)
        self.auth_header = 'Basic %s' % auth64

    def _build_url(self, url_suffix):
        return urlparse.urljoin(self.url_prefix, url_suffix)

    def _do_request(self, url, data):
        response = requests.post(url=url,
            headers={
                'Authorization':self.auth_header,
                'content-type': 'application/json'
            },
            data=json.dumps(data),
        )
        try:
            resp_data = response.json()

            # Raise JSON-bearing exception on error
            errors = resp_data.get('Errors')
            if errors:
                raise EWayError(
                    "API error response %r see `json_data`" % errors,
                    json_data=resp_data)

            return resp_data
        except Exception, e:
            raise e

    def create_access_code(self, redirect_url, client_ip,
                           customer=None, payment=None, shipping_address=None,
                           items=[], options=[], ):
        return self._create_or_update_access_code(
            'create', redirect_url, client_ip, customer=customer,
            payment=payment, shipping_address=shipping_address,
            items=items, options=options)

    def update_access_code(self, redirect_url, client_ip,
                           customer=None, payment=None, shipping_address=None,
                           items=[], options=[], ):
        return self._create_or_update_access_code(
            'update', redirect_url, client_ip, customer=customer,
            payment=payment, shipping_address=shipping_address,
            items=items, options=options)

    def _create_or_update_access_code(
            self, operation, redirect_url, client_ip, customer=None,
            payment=None, shipping_address=None, items=[], options=[]):
        if operation == 'create':
            url = self._build_url('CreateAccessCode.json')
        elif operation == 'update':
            url = self._build_url('UpdateAccessCode.json')
        else:
            raise ValueError("operation must be 'create' or 'update': %s"
                             % operation)
        data = {
            "RedirectUrl": redirect_url,
            "Method": "ProcessPayment",
            "CustomerIP": client_ip
        }
        # build a data dict from the passed in parameters
        if customer:
            data['Customer'] = customer.data
        if shipping_address:
            data['ShippingAddress'] = shipping_address.data
        if payment:
            data['Payment'] = payment.data
        if items:
            data['Items'] = []
            for item in items:
                data['Items'].append(item.data)
        if options:
            data['Options'] = []
            for option in options:
                data['Options'].append(option.data)

#        data = {
#                "Customer": {
#                    "Reference": "A12345",
#                    "Title": "Mr.",
#                    "FirstName": "John",
#                    "LastName": "Smith",
#                    "CompanyName": "Demo Shop 123",
#                    "JobDescription": "Developer",
#                    "Street1": "Level 5",
#                    "Street2": "369 Queen Street",
#                    "City": "Auckland",
#                    "State": "",
#                    "PostalCode": "1010",
#                    "Country": "nz",
#                    "Email": "sales@demoshop123.com",
#                    "Phone": "09 889 0986",
#                    "Mobile": "09 889 0986"
#                },
#                "ShippingAddress": {
#                    "ShippingMethod": "NextDay",
#                    "FirstName": "John",
#                    "LastName": "Smith",
#                    "Street1": "Level 5",
#                    "Street2": "369 Queen Street",
#                    "City": "Auckland",
#                    "State": "",
#                    "Country": "nz",
#                    "PostalCode": "1010",
#                    "Email": "sales@demoshop123.com",
#                    "Phone": "09 889 0986"
#                },
#                "Items": [
#                        {
#                            "SKU": "SKU1",
#                            "Description": "Description1"
#                        },
#                        {
#                            "SKU": "SKU2",
#                            "Description": "Description2"
#                        },
#                        {
#                            "SKU": "SKU3",
#                            "Description": "Description3"
#                        }
#                    ],
#                "Options": [
#                        {
#                            "Value": "Option1" },
#                        {
#                            "Value": "Option2"
#                        }, {
#                            "Value": "Option3" }
#                    ],
#                "Payment": {
#                    "TotalAmount": 100.05,
#                    "InvoiceNumber": "Inv 21540",
#                    "InvoiceDescription": "Individual Invoice Description",
#                    "InvoiceReference": "513456",
#                    "CurrencyCode": "AUD"
#                },
#                "RedirectUrl": "http://mysite.co.nz/Results",
#                "Method": "ProcessPayment",
#                "DeviceID": "D1234",
#                "CustomerIP": "127.0.0.1"
#            }

        resp_data = self._do_request(url, data)
        return resp_data['AccessCode'], resp_data['FormActionURL']

    def get_access_code_result(self, access_code):
        # using the given access code, make a request to eway to get
        # the outcome of a transaction.
        url = self._build_url('GetAccessCodeResult.json')
        data = {
            "AccessCode": access_code,
            }
        return self._do_request(url, data)
